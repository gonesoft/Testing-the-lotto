﻿using System;
using System.Collections.Generic;

namespace TestingTheLoto
{
    class Combinations
    {
        #region ( Variaciones con repeticion )
        //Portal
        public static List<T[]> VarConRep<T>(T[] original, int largo)
        {
            List<T[]> lista = new List<T[]>();
            ImplementVarConRep<T>(original, new T[largo], lista, 0);
            return lista;
        }
        //Recursivo
        private static void ImplementVarConRep<T>(T[] original, T[] temp, List<T[]> lista, int pos)
        {
            if (pos == temp.Length)
            {
                T[] copia = new T[pos];
                Array.Copy(temp, copia, pos);
                lista.Add(copia);
            }
            else
                for (int i = 0; i < original.Length; i++)
                {
                    temp[pos] = original[i];
                    ImplementVarConRep<T>(original, temp, lista, pos + 1);
                }
        }
        #endregion

        #region ( Variaciones sin repeticion )
        //Portal
        public static List<T[]> VarSinRep<T>(T[] original, int largo)
        {
            List<T[]> lista = new List<T[]>();
            ImplementVarSinRep<T>(original, 0, largo, lista);
            return lista;
        }
        //Recursivo
        private static void ImplementVarSinRep<T>(T[] original, int pos, int largo, List<T[]> lista)
        {
            if (pos == largo)
            {
                T[] copia = new T[pos];
                Array.Copy(original, copia, pos);
                lista.Add(copia);
            }
            else
                for (int i = pos; i < original.Length; i++)
                {
                    Swap(ref original[i], ref original[pos]);
                    ImplementVarSinRep<T>(original, pos + 1, largo, lista);
                    Swap(ref original[i], ref original[pos]);
                }
        }
        //Cambia
        private static void Swap<T>(ref T p, ref T p_2)
        {
            T aux = p;
            p = p_2;
            p_2 = aux;
        }
        #endregion

        #region ( Combinaciones )
        public static List<T[]> Combinaciones<T>(T[] original, int largo)
        {
            List<T[]> lista = new List<T[]>();
            ImplementCombinaciones<T>(original, new T[largo], 0, 0, lista);
            return lista;
        }
        private static void ImplementCombinaciones<T>(T[] original, T[] temp, int posorig, int postemp, List<T[]> lista)
        {
            if (postemp == temp.Length)
            {
                T[] copia = new T[postemp];
                Array.Copy(temp, copia, postemp);
                lista.Add(copia);
            }
            else if (posorig == original.Length) return;
            else
            {
                temp[postemp] = original[posorig];
                ImplementCombinaciones<T>(original, temp, posorig + 1, postemp + 1, lista);
                ImplementCombinaciones<T>(original, temp, posorig + 1, postemp, lista);
            }
        }

        public static void ImplementCombinaciones(int[] original, int[] temp, int posorig, int postemp,
            List<ValoresPeso> listaValores, List<ValorNumeroMa> listaValoresMas, DateTime datetime)
        {
            if (postemp == temp.Length)
            {
                int[] copia = new int[postemp];
                Array.Copy(temp, copia, postemp);
                addNumbers(copia, listaValores, listaValoresMas, datetime, postemp);
            }
            else if (posorig == original.Length) return;
            else
            {
                temp[postemp] = original[posorig];
                ImplementCombinaciones(original, temp, posorig + 1, postemp + 1, listaValores, listaValoresMas, datetime);
                ImplementCombinaciones(original, temp, posorig + 1, postemp, listaValores, listaValoresMas, datetime);
            }
        }
        #endregion

        #region ( Print )
        public static void Imprime<T>(List<T[]> lista)
        {
            for (int i = 0; i < lista.Count; i++)
            {
                for (int j = 0; j < lista[i].Length; j++)
                    Console.Write(lista[i][j] + ".");
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("Cantidad: {0}", lista.Count);
        }
        #endregion


        public static void addNumbers(int[] comb, List<ValoresPeso> valoresList, 
            List<ValorNumeroMa> valoresMasList, DateTime d, int postemp)
        {

            using (var con = new LotoStaticsEntities())
            {
                double total = 0;
                for (int i = 0; i < comb.Length; i++)
                {
                    total += valoresList.Find(x => x.Numero == comb[i]).Valor;
                }
                CombinacionesPeso p = new CombinacionesPeso()
                {
                    Primera = comb[0],
                    Segunda = comb[1],
                    Tercera = comb[2],
                    Cuarta = comb[3],
                    Quinta = comb[4],
                    Sexta = comb[5],
                    Peso_Total = total,
                    fecha_agregado = d
                };

                con.CombinacionesPesos.Add(p);
                con.SaveChanges();
                List<CombinacionesPesosMa> cpm = new List<CombinacionesPesosMa>();

                for (int i = 1; i <= 10; i++)
                {
                    cpm.Add(new CombinacionesPesosMa
                    {

                        Septima = i,
                        Peso_Total_Mas =  total + valoresMasList.Find(x => x.Numero == i).Valor,
                        CombinacionesPeso_Id = p.Id,
                        fecha_agregado = d
                    
                    });

                }
                con.CombinacionesPesosMas.AddRange(cpm);
                con.SaveChanges();


                Console.Clear();
                Console.Write("/ {0}", postemp);
                Console.Clear();
                Console.Write("-");
                Console.Clear();
                Console.Write("\\ {0}", postemp);
            }
        }

    }
}
