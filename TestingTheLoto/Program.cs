﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;



namespace TestingTheLoto
{
    class Program
    {

        static void Main(string[] args)
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            System.Threading.Thread.Sleep(500);
            List<ValoresPeso> listValores = new List<ValoresPeso>();
            List<ValorNumeroMa> listValoresMas = new List<ValorNumeroMa>();
            DateTime d = DateTime.Now;

            using (var con = new LotoStaticsEntities())
            {
                listValores = con.ValoresPesoes.ToList();
                listValoresMas = con.ValorNumeroMas.ToList();
            }

            Combinations.ImplementCombinaciones(
                new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                    24, 25 ,26 ,27 ,28, 29 ,30 ,31, 32, 33, 34, 35, 36, 37, 38 }, new int[6], 0, 0, listValores, listValoresMas, d);

            //Imprime<int>(Combinaciones<int>(new int[] { 13,15,17,27,30,3 }, 5));
            s.Stop();
            double factorial = (38 * 37 * 36 * 35 * 34 * 33) / (6 * 5 * 4 * 3 * 2 * 1);
            Console.WriteLine("Cantidad total Correcta: " + factorial);
            Console.WriteLine("Tiempo transcurrido: " + s.Elapsed);


            Console.ReadKey();
        }
    }
}
